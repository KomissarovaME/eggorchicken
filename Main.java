package eggOrChicken;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        DisputeThread egg = new DisputeThread("яйцо");
        DisputeThread chicken = new DisputeThread("курица");

        egg.start();
        chicken.start();
        try {
            chicken.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (egg.isAlive()) {
            egg.join();
            System.out.println("Выиграло яйцо.");
        } else {
            System.out.println("Выиграла курица.");
        }
    }
}
